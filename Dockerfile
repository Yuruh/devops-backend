FROM golang

WORKDIR /go/src/backend

ADD . .

RUN go get github.com/pilu/fresh

RUN go get

CMD fresh
