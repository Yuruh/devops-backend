package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gopkg.in/mgo.v2"
	"log"
	"net/http"
	"os"
)

func increment(resWriter http.ResponseWriter , req *http.Request) {
	resWriter.Header().Set("Content-Type", "application/json")
	json.NewEncoder(resWriter).Encode("test")
}

func main() {
	fmt.Printf("hello world, what's up ?\n")

	session, err := mgo.Dial("db:27017/test_db")
	defer session.Close()
	if err != nil {
		log.Fatalln("Could not connect to mongo db", err)
		os.Exit(1)
	} else {
		log.Println("Connected to db")
	}
	router := mux.NewRouter()
	router.HandleFunc("/increment", increment).
		Methods("POST")
	http.ListenAndServe(":8080", cors.AllowAll().Handler(router))
	log.Println("Listening on port 8080")
}